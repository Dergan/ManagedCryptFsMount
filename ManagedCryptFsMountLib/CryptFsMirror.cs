﻿using DokanNet;
using DokanNet.Logging;
using ManagedCryptFs;
using ManagedCryptFs.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using FileAccess = DokanNet.FileAccess;

namespace ManagedCryptFsMountLib
{
    public class CryptFsMirror : IDokanOperations
    {
        private readonly string SourcePath;
        private PrivateKeyFile keyFile;
        public const string CryptFsExtention = ".mfs";
        private const int CACHE_LIMIT = 100000;

        private const FileAccess DataAccess = FileAccess.ReadData | FileAccess.WriteData | FileAccess.AppendData |
                                              FileAccess.Execute |
                                              FileAccess.GenericExecute | FileAccess.GenericWrite | FileAccess.GenericRead;

        private const FileAccess DataWriteAccess = FileAccess.WriteData | FileAccess.AppendData |
                                                   FileAccess.Delete |
                                                   FileAccess.GenericWrite;

        private ConsoleLogger logger = new ConsoleLogger("[Mirror] ");

        private SortedList<string, FileInfoCache> FileCache;
        public bool RenameEncryptedFiles { get; set; }
        public string EncryptedFilePrefix { get; set; }
        public int RenameFileLength { get; set; }

        private object IOLock = new object();

        public CryptFsMirror(string path, PrivateKeyFile keyFile)
        {
            if (!System.IO.Directory.Exists(path))
                throw new ArgumentException("path");

            this.SourcePath = path;
            this.keyFile = keyFile;
            this.FileCache = new SortedList<string, FileInfoCache>();
            this.EncryptedFilePrefix = "enc_";
            this.RenameFileLength = 5;
        }

        private string GetPath(string fileName)
        {
            if (SourcePath.EndsWith("\\"))
                return SourcePath + fileName;

            return SourcePath + "\\" + fileName;
        }

        private string GetPath(string path, string fileName)
        {
            if (path.EndsWith("\\"))
                return SourcePath + path + fileName;

            return SourcePath + path + "\\" + fileName;
        }

        private string ToTrace(DokanFileInfo info)
        {
            var context = info.Context != null ? "<" + info.Context.GetType().Name + ">" : "<null>";

            return string.Format(CultureInfo.InvariantCulture, "{{{0}, {1}, {2}, {3}, {4}, #{5}, {6}, {7}}}",
                context, info.DeleteOnClose, info.IsDirectory, info.NoCache, info.PagingIo, info.ProcessId, info.SynchronousIo, info.WriteToEndOfFile);
        }

        private string ToTrace(DateTime? date)
        {
            return date.HasValue ? date.Value.ToString(CultureInfo.CurrentCulture) : "<null>";
        }

        private NtStatus Trace(string method, string fileName, DokanFileInfo info, NtStatus result, params string[] parameters)
        {
            var extraParameters = parameters != null && parameters.Length > 0 ? ", " + string.Join(", ", parameters) : string.Empty;

#if TRACE
            logger.Debug(string.Format(CultureInfo.InvariantCulture, "{0}('{1}', {2}{3}) -> {4}",
                method, fileName, ToTrace(info), extraParameters, result));
#endif

            return result;
        }

        private NtStatus Trace(string method, string fileName, DokanFileInfo info,
                                  FileAccess access, System.IO.FileShare share, System.IO.FileMode mode, System.IO.FileOptions options, System.IO.FileAttributes attributes,
                                  NtStatus result)
        {
#if TRACE
            logger.Debug(string.Format(CultureInfo.InvariantCulture, "{0}('{1}', {2}, [{3}], [{4}], [{5}], [{6}], [{7}]) -> {8}",
                 method, fileName, ToTrace(info), access, share, mode, options, attributes, result));
#endif

            return result;
        }

        #region Implementation of IDokanOperations

        public NtStatus CreateFile(string fileName, FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info)
        {
            var path = GetPath(fileName);

            if (info.IsDirectory)
            {
                try
                {
                    switch (mode)
                    {
                        case FileMode.Open:
                        {
                            if (!Directory.Exists(path))
                            {
                                try
                                {
                                    if (!File.GetAttributes(path).HasFlag(FileAttributes.Directory))
                                        return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, NtStatus.NotADirectory);
                                }
                                catch (Exception)
                                {
                                    return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.FileNotFound);
                                }
                                return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.PathNotFound);
                            }

                            new DirectoryInfo(path).EnumerateFileSystemInfos().Any(); // you can't list the directory
                            break;
                        }

                        case FileMode.CreateNew:
                        {
                            if (Directory.Exists(path))
                                return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.FileExists);

                            try
                            {
                                File.GetAttributes(path).HasFlag(FileAttributes.Directory);
                                return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.AlreadyExists);
                            }
                            catch (IOException) { }

                            Directory.CreateDirectory(GetPath(fileName));
                            break;
                        }
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.AccessDenied);
                }
            }
            else
            {
                bool pathExists = true;
                bool pathIsDirectory = false;

                bool readWriteAttributes = (access & DataAccess) == 0;
                bool readAccess = (access & DataWriteAccess) == 0;
                
                //grab the file from cache or find it on disk
                //we need to find the file incase the Rename file feature was used
                FileInfoCache cache = GetFile(path);
                if (cache != null)
                {
                    path = cache.FullPath;
                }

                try
                {
                    pathExists = (Directory.Exists(path) || File.Exists(path));
                    pathIsDirectory = pathExists && File.GetAttributes(path).HasFlag(FileAttributes.Directory);
                }
                catch (IOException) { }

                switch (mode)
                {
                    case FileMode.Open:
                    {
                        //string TargetFile = FindEncryptedFile(path.Substring(0, path.LastIndexOf('\\')), path.Substring(path.LastIndexOf('\\') + 1));

                        if (pathExists)
                        {
                            if (readWriteAttributes || pathIsDirectory)
                            // check if driver only wants to read attributes, security info, or open directory
                            {
                                if (pathIsDirectory && (access & FileAccess.Delete) == FileAccess.Delete
                                    && (access & FileAccess.Synchronize) != FileAccess.Synchronize) //It is a DeleteFile request on a directory
                                    return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.AccessDenied);

                                info.IsDirectory = pathIsDirectory;
                                info.Context = new object();
                                // must set it to someting if you return DokanError.Success

                                return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.Success);
                            }
                        }
                        else
                        {
                            return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.FileNotFound);
                        }
                        break;
                    }
                    case FileMode.CreateNew:
                    {
                        if (pathExists)
                        {
                            return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.FileExists);
                        }
                        break;
                    }
                    case FileMode.Truncate:
                    {
                        if (!pathExists)
                        {
                            return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.FileNotFound);
                        }
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }

                try
                {
                    string DestFileNameOnDisk = path;

                    if (mode == FileMode.CreateNew || mode == FileMode.Create && !pathExists)
                    {
                        string Filename = path.Substring(path.LastIndexOf('\\') + 1);
                        
                        if (RenameEncryptedFiles && !path.EndsWith(CryptFsExtention))
                        {
                            string FilePath = path.Substring(0, path.LastIndexOf('\\')) + "\\";
                            string RandFileName = "";

                            do
                            {
                                RandFileName = GetRandomFileName(RenameFileLength);
                            } while (File.Exists(FilePath + RandFileName));
                            DestFileNameOnDisk = FilePath + RandFileName;
                        }

                        //create a new encrypted file
                        using (FileStream TempStream = new FileStream(DestFileNameOnDisk, mode, System.IO.FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            EncryptedFile.EncryptFile(new MemoryStream(), TempStream, Filename, this.keyFile);
                        }

                        if (mode == FileMode.CreateNew || mode == FileMode.Create) //Files are always created as Archive
                            attributes |= FileAttributes.Archive;

                        File.SetAttributes(DestFileNameOnDisk, attributes);
                    }

                    if (mode == FileMode.Open || mode == FileMode.CreateNew || mode == FileMode.Create)
                    {
                        lock (FileCache)
                        {
                            if (FileCache.TryGetValue(DestFileNameOnDisk, out cache))
                            {
                                info.Context = cache;
                            }
                            else
                            {
                                cache = new FileInfoCache(DestFileNameOnDisk, keyFile);
                                FileCache.Add(DestFileNameOnDisk, cache);
                                info.Context = cache;
                            }
                        }

                        info.Context = GetFile(path);
                    }
                }
                catch (UnauthorizedAccessException) // don't have access rights
                {
                    return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.AccessDenied);
                }
                catch (DirectoryNotFoundException)
                {
                    return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.PathNotFound);
                }
                catch (Exception ex)
                {
                    uint hr = (uint)Marshal.GetHRForException(ex);
                    switch (hr)
                    {
                        case 0x80070020: //Sharing violation
                        return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.SharingViolation);
                        default:
                        throw ex;
                    }
                }
            }
            return Trace("CreateFile", fileName, info, access, share, mode, options, attributes, DokanResult.Success);
        }

        public void Cleanup(string fileName, DokanFileInfo info)
        {
            try
            {
#if TRACE
                if (info.Context != null)
                {
                    Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "{0}('{1}', {2} - entering", "Cleanup", fileName, ToTrace(info)));
                }
#endif

                /*if (info.Context != null && info.Context is EncryptedFile)
                {
                    (info.Context as EncryptedFile).Dispose();
                }
                info.Context = null;*/

                if (info.DeleteOnClose)
                {
                    string FullPath = GetPath(fileName);
                    if (info.IsDirectory)
                    {
                        Directory.Delete(FullPath);
                    }
                    else
                    {
                        lock (FileCache)
                        {
                            FileInfoCache cache = null;
                            if (FileCache.TryGetValue(FullPath, out cache))
                            {
                                cache.Dispose();
                                FileCache.Remove(FullPath);
                            }
                        }

                        File.Delete(FullPath);
                    }
                }
                Trace("Cleanup", fileName, info, DokanResult.Success);
            }
            catch (Exception ex)
            {

            }
        }

        public void CloseFile(string fileName, DokanFileInfo info)
        {
            try
            {
#if TRACE
                if (info.Context != null)
                {
                    Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "{0}('{1}', {2} - entering", "CloseFile", fileName, ToTrace(info)));
                }
#endif

                /*if (info.Context != null && info.Context is EncryptedFile)
                {
                    (info.Context as EncryptedFile).Dispose();
                }
                info.Context = null;*/
                Trace("CloseFile", fileName, info, DokanResult.Success); // could recreate cleanup code here but this is not called sometimes
            }
            catch (Exception ex)
            {

            }
        }

        public NtStatus ReadFile(string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info)
        {
            bytesRead = 0;

            try
            {
                lock (IOLock)
                {
                    string FullPath = GetPath(fileName);
                    FileInfoCache cache = GetFile(FullPath);

                    if (cache != null)
                    {
                        cache.EncStream.Position = offset;
                        bytesRead = cache.EncStream.Read(buffer, 0, buffer.Length);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Trace("ReadFile", fileName, info, DokanResult.Success, "out " + bytesRead.ToString(), offset.ToString(CultureInfo.InvariantCulture));
        }

        public NtStatus WriteFile(string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info)
        {
            bytesWritten = 0;

            try
            {
                lock (IOLock)
                {
                    string FullPath = GetPath(fileName);
                    FileInfoCache cache = GetFile(FullPath);

                    if (cache != null)
                    {
                        cache.EncStream.Position = offset;
                        cache.EncStream.Write(buffer, 0, buffer.Length);
                        bytesWritten = buffer.Length;
                    }
                }
            }
            catch (Exception ex)
            {
                //non-negative number,
            }

            /*if (info.Context == null)
            {
                using (var stream = new FileStream(GetPath(fileName), FileMode.Open, System.IO.FileAccess.Write))
                {
                    stream.Position = offset;
                    stream.Write(buffer, 0, buffer.Length);
                    bytesWritten = buffer.Length;
                }
            }
            else
            {
                var stream = info.Context as FileStream;
                lock (stream) //Protect from overlapped write
                {
                    stream.Position = offset;
                    stream.Write(buffer, 0, buffer.Length);
                }
                bytesWritten = buffer.Length;
            }*/
            return Trace("WriteFile", fileName, info, DokanResult.Success, "out " + bytesWritten.ToString(), offset.ToString(CultureInfo.InvariantCulture));
        }

        public NtStatus FlushFileBuffers(string fileName, DokanFileInfo info)
        {
            try
            {
                //((FileInfoCache)(info.Context)).Flush();
                return Trace("FlushFileBuffers", fileName, info, DokanResult.Success);
            }
            catch (IOException)
            {
                return Trace("FlushFileBuffers", fileName, info, DokanResult.DiskFull);
            }
        }

        public NtStatus GetFileInformation(string fileName, out FileInformation fileInfo, DokanFileInfo info)
        {
            lock (IOLock)
            {
                // may be called with info.Context == null, but usually it isn't
                string path = GetPath(fileName);


                //grab the file from cache or find it on disk
                //we need to find the file incase the Rename file feature was used
                FileInfoCache cache = GetFile(path);
                if (cache != null)
                {
                    path = cache.FullPath;
                }

                FileSystemInfo finfo = new FileInfo(path);
                if (!finfo.Exists)
                    finfo = new DirectoryInfo(path);

                string FullPath = path.Substring(0, path.LastIndexOf('\\'));

                fileInfo = new FileInformation
                {
                    FileName = fileName,
                    Attributes = finfo.Attributes,
                    CreationTime = finfo.CreationTime,
                    LastAccessTime = finfo.LastAccessTime,
                    LastWriteTime = finfo.LastWriteTime,
                    Length = (finfo is FileInfo) ? GetFileSize(FullPath + "\\" + finfo.Name, ((FileInfo)finfo).Length) : 0,
                };
                return Trace("GetFileInformation", fileName, info, DokanResult.Success);
            }
        }

        private static IList<FileInformation> GetEmptyDirectoryDefaultFiles()
            => new[] {
                new FileInformation() { FileName = ".", Attributes = FileAttributes.Directory, CreationTime = DateTime.Today, LastWriteTime = DateTime.Today, LastAccessTime = DateTime.Today },
                new FileInformation() { FileName = "..", Attributes = FileAttributes.Directory, CreationTime = DateTime.Today, LastWriteTime = DateTime.Today, LastAccessTime = DateTime.Today }
            };

        public NtStatus FindFiles(string fileName, out IList<FileInformation> files, DokanFileInfo info)
        {
            //This fonction is not called because FindFilesWithPattern is implemented
            // Return DokanResult.NotImplemented in FindFilesWithPattern to make FindFiles called
            files = files = FindFilesHelper(fileName, "*");

            return Trace("FindFiles", fileName, info, DokanResult.Success);
        }

        public NtStatus SetFileAttributes(string fileName, FileAttributes attributes, DokanFileInfo info)
        {
            try
            {
                FileInfoCache cache = GetFile(GetPath(fileName));
                if (cache != null)
                {
                    File.SetAttributes(cache.FullPath, attributes);
                }

                return Trace("SetFileAttributes", fileName, info, DokanResult.Success, attributes.ToString());
            }
            catch (UnauthorizedAccessException)
            {
                return Trace("SetFileAttributes", fileName, info, DokanResult.AccessDenied, attributes.ToString());
            }
            catch (FileNotFoundException)
            {
                return Trace("SetFileAttributes", fileName, info, DokanResult.FileNotFound, attributes.ToString());
            }
            catch (DirectoryNotFoundException)
            {
                return Trace("SetFileAttributes", fileName, info, DokanResult.PathNotFound, attributes.ToString());
            }
        }

        public NtStatus SetFileTime(string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info)
        {
            try
            {
                string path = GetPath(fileName);

                if (creationTime.HasValue)
                    File.SetCreationTime(path, creationTime.Value);

                if (lastAccessTime.HasValue)
                    File.SetLastAccessTime(path, lastAccessTime.Value);

                if (lastWriteTime.HasValue)
                    File.SetLastWriteTime(path, lastWriteTime.Value);

                return Trace("SetFileTime", fileName, info, DokanResult.Success, ToTrace(creationTime), ToTrace(lastAccessTime), ToTrace(lastWriteTime));
            }
            catch (UnauthorizedAccessException)
            {
                return Trace("SetFileTime", fileName, info, DokanResult.AccessDenied, ToTrace(creationTime), ToTrace(lastAccessTime), ToTrace(lastWriteTime));
            }
            catch (FileNotFoundException)
            {
                return Trace("SetFileTime", fileName, info, DokanResult.FileNotFound, ToTrace(creationTime), ToTrace(lastAccessTime), ToTrace(lastWriteTime));
            }
        }

        public NtStatus DeleteFile(string fileName, DokanFileInfo info)
        {
            var path = GetPath(fileName);

            if (!File.Exists(path))
                return Trace("DeleteFile", fileName, info, DokanResult.FileNotFound);

            if (File.GetAttributes(path).HasFlag(FileAttributes.Directory))
                return Trace("DeleteFile", fileName, info, DokanResult.AccessDenied);

            return Trace("DeleteFile", fileName, info, DokanResult.Success);
            // we just check here if we could delete the file - the true deletion is in Cleanup
        }

        public NtStatus DeleteDirectory(string fileName, DokanFileInfo info)
        {
            return Trace("DeleteDirectory", fileName, info, Directory.EnumerateFileSystemEntries(GetPath(fileName)).Any() ? DokanResult.DirectoryNotEmpty : DokanResult.Success);
            // if dir is not empty it can't be deleted
        }

        public NtStatus MoveFile(string oldName, string newName, bool replace, DokanFileInfo info)
        {
            string oldpath = GetPath(oldName);
            string newpath = GetPath(newName);

            /*if (info.Context != null && info.Context is EncryptedFile)
            {
                (info.Context as EncryptedFile).Dispose();
            }
            info.Context = null;*/

            bool exist = false;
            if (info.IsDirectory)
                exist = Directory.Exists(newpath);
            else
                exist = File.Exists(newpath);

            if (!exist)
            {
                info.Context = null;
                if (info.IsDirectory)
                    Directory.Move(oldpath, newpath);
                else
                    File.Move(oldpath, newpath);
                return Trace("MoveFile", oldName, info, DokanResult.Success, newName, replace.ToString(CultureInfo.InvariantCulture));
            }
            else if (replace)
            {
                info.Context = null;

                if (info.IsDirectory) //Cannot replace directory destination - See MOVEFILE_REPLACE_EXISTING
                    return Trace("MoveFile", oldName, info, DokanResult.AccessDenied, newName, replace.ToString(CultureInfo.InvariantCulture));

                File.Delete(newpath);
                File.Move(oldpath, newpath);
                return Trace("MoveFile", oldName, info, DokanResult.Success, newName, replace.ToString(CultureInfo.InvariantCulture));
            }
            return Trace("MoveFile", oldName, info, DokanResult.FileExists, newName, replace.ToString(CultureInfo.InvariantCulture));
        }

        public NtStatus SetEndOfFile(string fileName, long length, DokanFileInfo info)
        {
            return NtStatus.Success;
            /*try
            {
                ((FileStream)(info.Context)).SetLength(length);
                return Trace("SetEndOfFile", fileName, info, DokanResult.Success, length.ToString(CultureInfo.InvariantCulture));
            }
            catch (IOException)
            {
                return Trace("SetEndOfFile", fileName, info, DokanResult.DiskFull, length.ToString(CultureInfo.InvariantCulture));
            }*/
        }

        public NtStatus SetAllocationSize(string fileName, long length, DokanFileInfo info)
        {
            return NtStatus.Success;

            /*try
            {
                ((FileStream)(info.Context)).SetLength(length);
                return Trace("SetAllocationSize", fileName, info, DokanResult.Success, length.ToString(CultureInfo.InvariantCulture));
            }
            catch (IOException)
            {
                return Trace("SetAllocationSize", fileName, info, DokanResult.DiskFull, length.ToString(CultureInfo.InvariantCulture));
            }*/
        }

        public NtStatus LockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            return NtStatus.Success;

            /*try
            {
                ((FileStream)(info.Context)).Lock(offset, length);
                return Trace("LockFile", fileName, info, DokanResult.Success, offset.ToString(CultureInfo.InvariantCulture), length.ToString(CultureInfo.InvariantCulture));
            }
            catch (IOException)
            {
                return Trace("LockFile", fileName, info, DokanResult.AccessDenied, offset.ToString(CultureInfo.InvariantCulture), length.ToString(CultureInfo.InvariantCulture));
            }*/
        }

        public NtStatus UnlockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            return NtStatus.Success;

            /*try
            {
                ((FileStream)(info.Context)).Unlock(offset, length);
                return Trace("UnlockFile", fileName, info, DokanResult.Success, offset.ToString(CultureInfo.InvariantCulture), length.ToString(CultureInfo.InvariantCulture));
            }
            catch (IOException)
            {
                return Trace("UnlockFile", fileName, info, DokanResult.AccessDenied, offset.ToString(CultureInfo.InvariantCulture), length.ToString(CultureInfo.InvariantCulture));
            }*/
        }

        public NtStatus GetDiskFreeSpace(out long free, out long total, out long used, DokanFileInfo info)
        {
            var dinfo = DriveInfo.GetDrives().Where(di => di.RootDirectory.Name == Path.GetPathRoot(SourcePath + "\\")).Single();

            used = dinfo.AvailableFreeSpace;
            total = dinfo.TotalSize;
            free = dinfo.TotalFreeSpace;
            return Trace("GetDiskFreeSpace", null, info, DokanResult.Success, "out " + free.ToString(), "out " + total.ToString(), "out " + used.ToString());
        }

        public NtStatus GetVolumeInformation(out string volumeLabel, out FileSystemFeatures features,
                                                out string fileSystemName, DokanFileInfo info)
        {
            volumeLabel = "ManagedCryptFs";
            fileSystemName = "ManagedCryptFs";

            features = FileSystemFeatures.CasePreservedNames | FileSystemFeatures.CaseSensitiveSearch |
                       FileSystemFeatures.PersistentAcls | FileSystemFeatures.SupportsRemoteStorage |
                       FileSystemFeatures.UnicodeOnDisk;

            return Trace("GetVolumeInformation", null, info, DokanResult.Success, "out " + volumeLabel, "out " + features.ToString(), "out " + fileSystemName);
        }

        public NtStatus GetFileSecurity(string fileName, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            try
            {
                FileInfoCache cache = GetFile(GetPath(fileName));

                security = info.IsDirectory
                               ? (FileSystemSecurity)Directory.GetAccessControl(fileName)
                               : File.GetAccessControl(cache.FullPath);
                return Trace("GetFileSecurity", fileName, info, DokanResult.Success, sections.ToString());
            }
            catch (UnauthorizedAccessException)
            {
                security = null;
                return Trace("GetFileSecurity", fileName, info, DokanResult.AccessDenied, sections.ToString());
            }
        }

        public NtStatus SetFileSecurity(string fileName, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            try
            {
                if (info.IsDirectory)
                {
                    Directory.SetAccessControl(GetPath(fileName), (DirectorySecurity)security);
                }
                else
                {
                    FileInfoCache cache = GetFile(GetPath(fileName));
                    File.SetAccessControl(cache.FullPath, (FileSecurity)security);
                }
                return Trace("SetFileSecurity", fileName, info, DokanResult.Success, sections.ToString());
            }
            catch (UnauthorizedAccessException)
            {
                return Trace("SetFileSecurity", fileName, info, DokanResult.AccessDenied, sections.ToString());
            }
        }

        public NtStatus Mounted(DokanFileInfo info)
        {
            return Trace("Mount", null, info, DokanResult.Success);
        }

        public NtStatus Unmounted(DokanFileInfo info)
        {
            return Trace("Unmount", null, info, DokanResult.Success);
        }

        public NtStatus FindStreams(string fileName, IntPtr enumContext, out string streamName, out long streamSize, DokanFileInfo info)
        {
            streamName = String.Empty;
            streamSize = 0;
            return Trace("EnumerateNamedStreams", fileName, info, DokanResult.NotImplemented, enumContext.ToString(), "out " + streamName, "out " + streamSize.ToString());
        }

        public NtStatus FindStreams(string fileName, out IList<FileInformation> streams, DokanFileInfo info)
        {
            streams = new FileInformation[0];
            return Trace("EnumerateNamedStreams", fileName, info, DokanResult.NotImplemented);
        }

        public IList<FileInformation> FindFilesHelper(string fileName, string searchPattern)
        {
            string FilePath = GetPath(fileName);
            FilePath = FilePath.Substring(0, FilePath.LastIndexOf('\\')) + "\\";

            IList<FileInformation> files = new DirectoryInfo(GetPath(fileName))
                //.GetFileSystemInfos(searchPattern)
                .GetFileSystemInfos()
                .Where(fileInfo =>
                {
                    if (searchPattern == "*")
                        return true;

                    searchPattern = searchPattern.Replace('<', '*');

                    FileInfoCache cache = GetFile(FilePath + fileInfo.Name);

                    if (searchPattern.Contains("*") && cache != null)
                    {
                        //implement search patterns
                        int LastDotIndex = searchPattern.LastIndexOf('.');

                        if (LastDotIndex > 0)
                        {
                            string extention = searchPattern.Substring(LastDotIndex);

                            if (cache.DecryptedFilename.ToLower().EndsWith(extention.ToLower()))
                            {
                                return true;
                            }
                        }

                        return false;
                    }

                    if (cache != null && cache.DecryptedFilename == searchPattern)
                        return true;

                    return false;
                })
                .Select(finfo => new FileInformation
                {
                    Attributes = finfo.Attributes,
                    CreationTime = finfo.CreationTime,
                    LastAccessTime = finfo.LastAccessTime,
                    LastWriteTime = finfo.LastWriteTime,
                    Length = (finfo is FileInfo) ? GetFileSize(GetPath(fileName, finfo.Name), ((FileInfo)finfo).Length) : 0,
                    FileName = GetFileName(GetPath(fileName, finfo.Name), finfo.Name)
                }).ToArray();

            if (fileName != "\\" && searchPattern == "*")  //Add current folder and parent folder when root directory is not requested
                files = GetEmptyDirectoryDefaultFiles().Concat(files).ToArray();

            return files;
        }

        public NtStatus FindFilesWithPattern(string fileName, string searchPattern, out IList<FileInformation> files, DokanFileInfo info)
        {
            files = FindFilesHelper(fileName, searchPattern);

            return Trace("FindFilesWithPattern", fileName, info, DokanResult.Success);
        }

        #endregion Implementation of IDokanOperations

        private long GetFileSize(string path, long OrgLength)
        {
            try
            {
                if (OrgLength > 0)
                {
                    return GetFile(path).FileSize;
                }
            }
            catch (Exception ex)
            {

            }
            return OrgLength;
        }

        private string GetFileName(string path, string OrgName)
        {
            try
            {
                FileInfoCache cache = GetFile(path);
                if (cache != null)
                {
                    return cache.EncFile.Header.Name;
                }
            }
            catch (Exception ex)
            {

            }
            return OrgName;
        }

        private string GetRandomFileName(int length)
        {
            SecureRandom rand = new SecureRandom();
            const string chars = "qazwsxedcrfvtgbyhnujmikolp0123456789";
            string ret = "";

            for (int i = 0; i < length; i++)
            {
                string c = chars[rand.Next(0, chars.Length)].ToString();

                if (rand.Next(0, 1) == 1)
                {
                    c = c.ToUpper();
                }
                ret += c;
            }

            return EncryptedFilePrefix + ret + CryptFsExtention;
        }

        private bool IsContextDisposed(object Context)
        {
            return Context as FileInfoCache == null;
        }

        private FileInfoCache GetFile(string Path)
        {
            lock (FileCache)
            {
                //it sometimes happens a double "\\" is used, need to fix this
                Path = Path.Replace("\\\\", "\\");

                string Filename = Path.Substring(Path.LastIndexOf('\\') + 1);
                string FilePath = Path.Substring(0, Path.LastIndexOf('\\')) + "\\";

                if (String.IsNullOrWhiteSpace(Filename) || (Directory.Exists(Path) && File.GetAttributes(Path).HasFlag(FileAttributes.Directory)))
                {
                    //empty file name is mostly a directory
                    return null;
                }

                //check if it exists in the cache first
                FileInfoCache cache = null;

                if (FileCache.TryGetValue(Path, out cache))
                    return cache;

                FileInfoCache val = FileCache.Values.FirstOrDefault(o => o.FullPath.StartsWith(FilePath) && (o.DecryptedFilename == Filename || o.EncryptedFilename == Filename));

                if (val != null)
                    return val;

                if (!RenameEncryptedFiles && File.Exists(Path))
                {
                    FileInfo inf = new FileInfo(Path);

                    if (inf.Length > 0)
                    {
                        cache = new FileInfoCache(Path, keyFile);
                        FileCache.Add(Path, cache);

                        if (FileCache.Count > CACHE_LIMIT)
                            FileCache.RemoveAt(0);
                    }
                }
                else //if (RenameEncryptedFiles)// && Filename.StartsWith(EncryptedFilePrefix) && Filename.EndsWith(CryptFsExtention))
                {
                    //look through the files in the specified directory

                    if (!Directory.Exists(FilePath))
                        return null;

                    foreach (string targetFile in Directory.GetFiles(FilePath))
                    {
                        try
                        {
                            string EncFilename = targetFile.Substring(targetFile.LastIndexOf('\\') + 1);
                            FileInfoCache tempCache = FileCache.Values.FirstOrDefault(o => o.EncryptedFilename == EncFilename);

                            if (tempCache == null)
                            {
                                FileInfo inf = new FileInfo(targetFile);

                                if (inf.Length == 0)
                                    continue;

                                //add the file to cache, add the decrypted path to the list
                                tempCache = new FileInfoCache(targetFile, keyFile);

                                if (!FileCache.ContainsKey(FilePath + tempCache.DecryptedFilename))
                                {
                                    FileCache.Add(FilePath + tempCache.DecryptedFilename, tempCache);

                                    if (FileCache.Count > CACHE_LIMIT)
                                        FileCache.RemoveAt(0);
                                }
                            }

                            if (tempCache.DecryptedFilename == Filename || tempCache.EncryptedFilename == Filename)
                            {
                                //found renamed/encrypted file
                                if (!FileCache.ContainsKey(FilePath + tempCache.DecryptedFilename))
                                {
                                    FileCache.Add(FilePath + tempCache.DecryptedFilename, tempCache);

                                    if (FileCache.Count > CACHE_LIMIT)
                                        FileCache.RemoveAt(0);
                                }

                                return tempCache;
                            }
                        }
                        catch(Exception ex)
                        {

                        }
                    }
                }

                return cache;
            }
        }
    }
}