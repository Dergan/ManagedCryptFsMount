﻿using ManagedCryptFs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ManagedCryptFsMountLib
{
    public class FileInfoCache : IDisposable
    {
        public string FullPath { get; private set; }
        public string FileName { get; private set; }
        public long FileSize
        {
            get
            {
                return EncStream.Length;
            }
        }

        public string EncryptedFilename { get; set; }
        public string DecryptedFilename { get; set; }

        public EncryptedFile EncFile { get; private set; }
        public EncryptedFileStream EncStream { get; private set; }

        public bool IsDisposed { get; private set; }

        public FileInfoCache(string Path, PrivateKeyFile keyFile)
        {
            this.FullPath = Path;
            this.FileName = FullPath.Substring(FullPath.LastIndexOf('\\') + 1);

            this.EncFile = EncryptedFile.OpenFile(new FileStream(Path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite), keyFile, 0);
            this.EncStream = new EncryptedFileStream(EncFile);
            EncryptedFilename = this.FileName;
            DecryptedFilename = EncFile.Header.Name;
        }

        public void Dispose()
        {
            EncStream.Dispose();
            EncFile.Dispose();
            IsDisposed = true;
        }
    }
}