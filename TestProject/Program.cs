﻿using DokanNet;
using ManagedCryptFs;
using ManagedCryptFsMountLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            string PrivateKeyFilePath = "./TestFiles/PrivateKeyFile.dat";
            string TestKeyFilePath = "./TestFiles/TempKeyFile.dat";
            string DataDirFilePath = Environment.CurrentDirectory + "\\EncryptedFiles";
            string TestDirFilePath = Environment.CurrentDirectory + "\\TestFiles";

            if (!Directory.Exists(DataDirFilePath))
                Directory.CreateDirectory(DataDirFilePath);

            if (!Directory.Exists(TestDirFilePath))
                Directory.CreateDirectory(TestDirFilePath);

            try
            {
                //unmount if it was mounted
                DokanNet.Dokan.Unmount('n');
            }
            catch { }

            Console.WriteLine("Unmounted the file system (if it was mounted)");

            PrivateKeyFile keyFile = new PrivateKeyFile();
            keyFile = PrivateKeyFile.GenerateKeyFile();

            if (!File.Exists(PrivateKeyFilePath))
            {
                //save the new key file if it did not exist
                Console.WriteLine("Generating new key file...");
                keyFile.EncryptKeyFile(new FileStream(PrivateKeyFilePath, FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, FileShare.ReadWrite), "y789w5yt7w58976y45", 1, new Stream[] { new FileStream(TestKeyFilePath, FileMode.Open, System.IO.FileAccess.Read, FileShare.Read) });
            }

            Console.WriteLine("Decrypting key file...");
            keyFile.DecryptKeyFile(new FileStream(PrivateKeyFilePath, FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, FileShare.ReadWrite), "y789w5yt7w58976y45", 1, new Stream[] { new FileStream(TestKeyFilePath, FileMode.Open, System.IO.FileAccess.Read, FileShare.Read) });

            Console.WriteLine("Decrypted key file");
            
            Console.WriteLine("Mounting file system");
            CryptFsMirror mirror = new CryptFsMirror(DataDirFilePath, keyFile);

            //optional settings for more security
            mirror.RenameFileLength = 5;
            mirror.RenameEncryptedFiles = false;

            mirror.Mount("n:\\", DokanOptions.DebugMode, 1);

            Process.GetCurrentProcess().WaitForExit();
        }
    }
}
